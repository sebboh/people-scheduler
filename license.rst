People Scheduler
================
`GPLv3 <legal/gpl-3.0.txt>`_

Pink-Calendar.png
=========================
`CC BY-SA 3.0 <legal/cc-by-sa-3.0-legalcode.txt>`_

Copyright 2012 wikipedia user ltzuvit.

See https://commons.wikimedia.org/wiki/File:Pink-Calendar.png
