People Scheduler
================
This project is at "brainstorming" stage.  There is no code here yet.

Issues
------
Please add Issues to the project.  An 'Issue' is a general-purpose note attached
to the project.  Issues can represent bug reports, feature requests, etc.

Issues can be opened, closed, commented upon, tagged, and more.  The list of
issues can be sorted, searched, etc.

The issues interface is one of the mechanisms that allows people to collaborate
on the project.